//
//  AppDelegate.h
//  Labb4
//
//  Created by Christian on 2016-02-17.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

