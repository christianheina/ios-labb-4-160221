//
//  PongViewController.h
//  Labb4
//
//  Created by Christian on 2016-02-18.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PongViewController : UIViewController
@property (nonatomic) BOOL gameStarted;
-(void)stopTimers;
-(void)startTimers;

@end
