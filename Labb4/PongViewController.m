//
//  PongViewController.m
//  Labb4
//
//  Created by Christian on 2016-02-18.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "PongViewController.h"
#import "AVFoundation/AVFoundation.h"

@interface PongViewController ()

@end

@implementation PongViewController{
    CGFloat screenWidth, screenHeight;
    UILabel *playerRedScoreLabel, *playerBlueScoreLabel, *gameInfoLabel;
    UIView *ball, *playerBlue, *playerRed;
    UIPanGestureRecognizer *pan;
    int playerBlueScore, playerRedScore, prepareGame, newXPosition;
    double bounceMultiplier;
    CGPoint newPan, oldPan, moveSpeed, oldBallPosition;
    NSTimer *ballMove, *prepareStart, *computerMove;
    AVAudioPlayer *bounce;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGSize size = bounds.size;
    screenWidth = size.width;
    screenHeight = size.height;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"short_ball_bounce" ofType:@"mp3"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:path];
    bounce = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    [bounce prepareToPlay];
    
    [self createLabels];
    
    pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onPan:)];
    [self setupGame];
}

-(void)createLabels{
    playerBlueScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, screenWidth/4, screenHeight/20)];
    [playerBlueScoreLabel setCenter:self.view.center];
    playerBlueScoreLabel.textColor = [UIColor blueColor];
    playerBlueScoreLabel.textAlignment = NSTextAlignmentCenter;
    playerBlueScoreLabel.center = CGPointMake(screenWidth-playerBlueScoreLabel.frame.size.width/2, playerBlueScoreLabel.center.y);
    [self.view addSubview:playerBlueScoreLabel];
    
    playerRedScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, screenWidth/4, screenHeight/20)];
    [playerRedScoreLabel setCenter:self.view.center];
    playerRedScoreLabel.textColor = [UIColor redColor];
    playerRedScoreLabel.textAlignment = NSTextAlignmentCenter;
    playerRedScoreLabel.center = CGPointMake(playerRedScoreLabel.frame.size.width/2, playerRedScoreLabel.center.y);
    [self.view addSubview:playerRedScoreLabel];
    
    gameInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, screenWidth/2, screenHeight/20)];
    [gameInfoLabel setCenter:self.view.center];
    gameInfoLabel.textColor = [UIColor blackColor];
    gameInfoLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:gameInfoLabel];
    
    ball = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth/20, screenWidth/20)];
    ball.backgroundColor = [UIColor blackColor];
    
    playerBlue = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth/5, screenWidth/20)];
    [playerBlue setCenter:self.view.center];
    [self.view addSubview:playerBlue];
    
    playerRed = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth/5, screenWidth/20)];
    [playerRed setCenter:self.view.center];
    [self.view addSubview:playerRed];
}

-(void)setupGame{
    playerBlue.backgroundColor = [UIColor blueColor];
    playerBlue.alpha = 0;
    playerRed.backgroundColor = [UIColor redColor];
    playerRed.alpha = 0;
    [ball removeFromSuperview];
    gameInfoLabel.hidden = NO;
    prepareStart = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(prepareGame:) userInfo:nil repeats:YES];
    [UIView animateWithDuration:2
                     animations:^{
                         playerBlue.center = CGPointMake(playerBlue.center.x, playerBlue.center.y+5*screenHeight/12);
                         playerRed.center = CGPointMake(playerRed.center.x, playerRed.center.y-5*screenHeight/12);
                         playerBlue.alpha = 1;
                         playerRed.alpha = 1;
                     } completion:^(BOOL finished){
                         [self.view addGestureRecognizer:pan];
                     }];
    
    playerBlueScore = 0;
    playerRedScore = 0;
    playerBlueScoreLabel.text = [NSString stringWithFormat:@"%d", playerBlueScore];
    playerRedScoreLabel.text = [NSString stringWithFormat:@"%d", playerRedScore];
}

-(void)fadeText{
    gameInfoLabel.hidden = NO;
    
    [UIView animateWithDuration:0.8
                     animations:^{
                         gameInfoLabel.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         gameInfoLabel.alpha = 1;
                         gameInfoLabel.hidden = YES;
                     }];
}

-(void)prepareNewRound{
    [self endRound];
    gameInfoLabel.hidden = NO;
    gameInfoLabel.text = [NSString stringWithFormat:@"Round %d", (playerBlueScore+playerRedScore+1)];
    [UIView animateWithDuration:1.5
                     animations:^{
                         gameInfoLabel.alpha = 0.0;
                         playerRed.center = CGPointMake(self.view.center.x, playerRed.center.y);
                     }
                     completion:^(BOOL finished){
                         gameInfoLabel.hidden = YES;
                         gameInfoLabel.alpha = 1;
                         [self startRound];
                     }];
}

-(void)startRound{
    self.gameStarted = YES;
    [ball setCenter:self.view.center];
    oldBallPosition = ball.center;
    moveSpeed = CGPointMake(screenWidth/50, screenWidth/50);
    moveSpeed = [self randomStartDirectionMoveSpeed];
    bounceMultiplier = 1.05;
    [self.view addSubview:ball];
    [self startTimers];
}

-(void)endRound{
    [self stopTimers];
    [ball removeFromSuperview];
}

-(CGPoint)randomStartDirectionMoveSpeed{
    int direction = arc4random()%4;
    int rand = 25 + arc4random()%30;
    double angle = (double)rand/100;
    if (direction == 0){
        double x = angle*moveSpeed.x;
        double y = (1-angle)*moveSpeed.y;
        return CGPointMake(x, y);
    } else if (direction == 1){
        double x = angle*moveSpeed.x;
        double y = (1-angle)*-moveSpeed.y;
        return CGPointMake(x, y);
    } else if (direction == 2){
        double x = angle*-moveSpeed.x;
        double y = (1-angle)*-moveSpeed.y;
        return CGPointMake(x, y);
    } else {
        double x = angle*-moveSpeed.x;
        double y = (1-angle)*moveSpeed.y;
        return CGPointMake(x, y);
    }
}

-(void)startTimers{
    ballMove = [NSTimer scheduledTimerWithTimeInterval:.01 target:self selector:@selector(moveAndCheckPosition:) userInfo:nil repeats:YES];
    computerMove = [NSTimer scheduledTimerWithTimeInterval:.09 target:self selector:@selector(computerMove:) userInfo:nil repeats:YES];
}

-(void)stopTimers{
    [ballMove invalidate];
    ballMove = nil;
    [computerMove invalidate];
    computerMove = nil;
}

-(void)prepareGame:(NSTimer*)aTimer{
    prepareGame++;
    if (prepareGame == 1){
        gameInfoLabel.text = @"Ready";
        [self fadeText];
    } else if (prepareGame == 2){
        gameInfoLabel.text = @"Set";
        [self fadeText];
    } else if (prepareGame == 3){
        gameInfoLabel.text = @"Go";
        [self fadeText];
        [prepareStart invalidate];
        prepareGame = 0;
        [self startRound];
    }
}

-(void)computerMove:(NSTimer*)aTimer{
    CGPoint ballPosition = CGPointMake(ball.center.x, ball.center.y);
    CGPoint computerPosition = CGPointMake(playerRed.center.x, playerRed.center.y);

    if (ballPosition.y > computerPosition.y+screenHeight/100){
        newXPosition = computerPosition.x+(ballPosition.x-computerPosition.x);
    } else if (ballPosition.y <= computerPosition.y+screenHeight/100 && ballPosition.x-computerPosition.x){
        int rand = arc4random()%3;
        if (rand != 0){
            newXPosition = computerPosition.x-(computerPosition.x-ballPosition.x);
        }
    }
    
    [UIView animateWithDuration:.09 animations:^{
        playerRed.center = CGPointMake(newXPosition, playerRed.center.y);
        if (playerRed.center.x <= 0){
            playerRed.center = CGPointMake(0, playerRed.center.y);
        } else if (playerRed.center.x >= screenWidth){
            playerRed.center = CGPointMake(screenWidth, playerRed.center.y);
        }
    }];
}

-(void)moveAndCheckPosition:(NSTimer*)aTimer{
    if (ball.center.x <= 0 || ball.center.x >= screenWidth){
        moveSpeed = CGPointMake(-moveSpeed.x, moveSpeed.y);
        [bounce setCurrentTime:0.0];
        [bounce play];
    }
    
    if (ball.center.y <= 0 || ball.center.y >= screenHeight){
        if (ball.center.y <= 0){
            playerBlueScore++;
            playerBlueScoreLabel.text = [NSString stringWithFormat:@"%d", playerBlueScore];
        } else if (ball.center.y >= screenHeight){
            playerRedScore ++;
            playerRedScoreLabel.text = [NSString stringWithFormat:@"%d", playerRedScore];
        }
        [self prepareNewRound];
        return;
    }
    
    if (CGRectContainsPoint(playerBlue.frame, ball.center)){
        [bounce setCurrentTime:0.0];
        [bounce play];
        if ((oldBallPosition.x <= playerBlue.frame.origin.x ||
             oldBallPosition.x >= playerBlue.frame.origin.x+playerBlue.frame.size.width) &&
            ball.center.y > playerBlue.frame.origin.y &&
            ball.center.y < playerBlue.frame.origin.y+playerBlue.frame.size.height) {
            //Player side bounce
            moveSpeed = CGPointMake(-moveSpeed.x, moveSpeed.y*bounceMultiplier);
        } else if (ball.center.x > playerBlue.frame.origin.x && ball.center.x < playerBlue.frame.origin.x+playerBlue.frame.size.width){
            if (ball.center.x < playerBlue.frame.origin.x+playerBlue.frame.size.width/10 ||
               ball.center.x > playerBlue.frame.origin.x+9*playerBlue.frame.size.width/10){
                //Player 10% each side edge bounce
                moveSpeed = CGPointMake(moveSpeed.x, moveSpeed.y*bounceMultiplier);
            } else {
                //Player middle 80% bounce
                int rand = arc4random()%2;
                if (rand == 0){
                    moveSpeed = CGPointMake(moveSpeed.x*bounceMultiplier, moveSpeed.y*bounceMultiplier);
                }else{
                    moveSpeed = CGPointMake(moveSpeed.x*bounceMultiplier, moveSpeed.y*bounceMultiplier);
                }
            }
        }
        //Player collided with ball, always make y go negative
        moveSpeed = CGPointMake(moveSpeed.x, -fabs(moveSpeed.y));
    } else if (CGRectContainsPoint(playerRed.frame, ball.center)){
        [bounce setCurrentTime:0.0];
        [bounce play];
        if ((oldBallPosition.x <= playerRed.frame.origin.x ||
             oldBallPosition.x >= playerRed.frame.origin.x+playerRed.frame.size.width) &&
            ball.center.y > playerRed.frame.origin.y &&
            ball.center.y < playerRed.frame.origin.y+playerRed.frame.size.height) {
            //Computer side bounce
            moveSpeed = CGPointMake(-moveSpeed.x, moveSpeed.y*bounceMultiplier);
        } else if (ball.center.x > playerRed.frame.origin.x && ball.center.x < playerRed.frame.origin.x+playerRed.frame.size.width){
            if (ball.center.x < playerRed.frame.origin.x+playerRed.frame.size.width/10 ||
               ball.center.x > playerRed.frame.origin.x+9*playerRed.frame.size.width/10){
                //Computer 10% each side edge bounce
                moveSpeed = CGPointMake(moveSpeed.x, moveSpeed.y*bounceMultiplier);
            } else {
                //Computer 80% middle bounce
                int rand = arc4random()%2;
                if (rand == 0){
                    moveSpeed = CGPointMake(moveSpeed.x*bounceMultiplier, moveSpeed.y*bounceMultiplier);
                }else{
                    moveSpeed = CGPointMake(moveSpeed.x*bounceMultiplier, moveSpeed.y*bounceMultiplier);
                }
            }
        }
        //Computer collided with ball, always make y go positive
        moveSpeed = CGPointMake(moveSpeed.x, fabs(moveSpeed.y));
    }
    
    if (sqrt(pow(moveSpeed.x*bounceMultiplier, 2) + pow(moveSpeed.y*bounceMultiplier, 2)) >= screenWidth/30){
        bounceMultiplier = 1;
    }
    
    [UIView animateWithDuration:.0099 animations:^{
        ball.center = CGPointMake(ball.center.x+moveSpeed.x, ball.center.y+moveSpeed.y);
    } completion:^(BOOL finished){
        oldBallPosition = ball.center;
    }];
}

-(IBAction)onPan:(UIPanGestureRecognizer*)sender{
    newPan = [sender locationInView:self.view];
    if (sender.state == UIGestureRecognizerStateBegan){
        oldPan = newPan;
    }
    playerBlue.center = CGPointMake(playerBlue.center.x-(oldPan.x-newPan.x), playerBlue.center.y);
    if (playerBlue.center.x <= 0){
        playerBlue.center = CGPointMake(0, playerBlue.center.y);
    } else if (playerBlue.center.x >= screenWidth){
        playerBlue.center = CGPointMake(screenWidth, playerBlue.center.y);
    }
    oldPan = newPan;
}

@end
